class DocumentsController < ApplicationController 

  layout 'portal'

  before_filter :authenticate_user!, :get_documents

  def index
      
  end

  def financials
    @documents = @documents.financials

    render action: 'index'
  end

  def reports
    @documents = @documents.reports

    render action: 'index'
  end

  def misc
    @documents = @documents.misc

    render action: 'index'
  end

  def download
    doc = Document.find(params[:id])
    send_data(doc.file_url, 
              :filename => doc.title, 
              :type => doc.content_type, 
              :disposition => :attachment, 
              :url_based_filename => false)
  end



  private

    def get_documents
        ids = MessageRecipient.where(:receiver_id => current_user.id).pluck(:message_id)

        @documents = Document.
          joins(:message_documents).
          where('message_documents.message_id IN (?)', ids).uniq.page(params[:page]).per(PER_PAGE)
    end

end
