class MessagesController < ApplicationController
	before_filter :authenticate_user!, :except => :index
  layout 'portal'

	def index
    if user_signed_in? && current_user
      redirect_to admin_messages_path if current_user.admin?
      
      rec_messages = current_user.received_messages.order("updated_at ASC")
      @received_messages = []
      rec_messages.each do |e|
        @received_messages << e.message
      end
    
      @messages = Kaminari.paginate_array(@received_messages).page(params[:page]).per(PER_PAGE)


    else
      redirect_to new_user_session_path
    end
	end

  def show

    @message = Message.find(params[:id])
    get_users_message_copy

    # mark users's message copy as read
    @recipient_copy.view
  end


  def hide
    # @message = Message.find(params[:id])
    get_users_message_copy


    if @recipient_copy.hide
      redirect_to portal_path, notice: 'Message has been archived!'
    else
      redirect_to portal_path, notice: 'Something went wrong!!'
    end
  end
	

  private

    def get_users_message_copy
      @recipient_copy = MessageRecipient.where(message_id: params[:id], receiver_id: current_user.id).first
    end
end
