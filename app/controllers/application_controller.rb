class ApplicationController < ActionController::Base
  PER_PAGE = '8'
  protect_from_forgery

  layout :layout_by_resource

  # Redirects to Investor Portal on successful signin
  def after_sign_in_path_for(resource)
   portal_path
  end
  # Redirects to Investor Portal on successful signout
  def after_sign_out_path_for(resource)
   new_user_session_path
  end

  rescue_from CanCan::AccessDenied do |exception|
    render :file => "#{Rails.root}/public/403", :formats => [:html], :status => 403, :layout => false
  end


  protected

  def layout_by_resource
    if devise_controller? && resource_name == :user && action_name == 'new'
      "application"
    elsif devise_controller? && resource_name == :user && action_name == 'edit'
      "portal"
    elsif devise_controller?  && action_name == 'edit'
      "application"
    elsif devise_controller? 
      "portal"
    else
      "application"
    end
  end
end
