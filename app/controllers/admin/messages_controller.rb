class Admin::MessagesController < ApplicationController
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
  #   IMPORTANT NOTE!
  # 
  #   The `load_and_authorize_resource` method automagically creates the instance 
  #   for each method below.
  # 
  #   They remain in place but commented out for the sake of being explicit
  # 
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

  load_and_authorize_resource

  layout 'portal'

  def index
    @messages = @messages.where(state: 'sent').order("updated_at DESC").page(params[:page]).per(PER_PAGE)
  end

  def new
    @users = User.all
    @message = Message.new
  end

  def create
    if @message.save
      @message.deliver
      redirect_to portal_path, notice: 'Message successfully sent!'
    else
      redirect_to new_admin_message_path, notice: 'Something went wrong..'
    end
  end

  def edit
    @users = User.all
  end

  def update
    if @message.update_attributes(params[:message])
      redirect_to admin_messages_path, notice: 'Message was successfully updated.'
    else
      render action: 'edit'
    end
  end

  def destroy
    if @message.destroy
      redirect_to admin_messages_path, notice: 'Message was successfully deleted.'
    else
      redirect_to admin_messages_path, notice: 'Something went wrong..'
    end
  end


end
