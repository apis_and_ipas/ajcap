class Admin::UsersController < ApplicationController
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
  #   IMPORTANT NOTE!
  # 
  #   The `load_and_authorize_resource` method automagically creates the instance 
  #   for each method below.
  # 
  #   They remain in place but commented out for the sake of being explicit
  # 
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
  load_and_authorize_resource
  
  layout 'portal'

  def index
    # @users = User.all  
    @users = @users.page(params[:page]).per(PER_PAGE)
  end

  def show
    # @user = User.find(params[:id])
  end

  def edit
    # @user = User.find(params[:id])
  end

  def update
    fields = params[:user]
    
    @user.is_admin   = fields.delete(:admin)
    @user.attributes = fields
    

    if @user.save
      redirect_to admin_users_path, notice: 'User was successfully updated.'
    else
      render action: 'edit', notice: 'Something when wrong with the record update'
    end

  end

  def new
    # @user = User.new
  end

  def create
    if @user.save
      redirect_to admin_user_path @user, notice: 'User was successfully created.'
    else
      render action: 'new'
    end
    
  end

  def destroy
    @user.destroy
    redirect_to admin_users_path, notice: 'User was successfully deleted.'
  end

  def user_params
    params.require(:user).permit(:email, :password)
  end
end
