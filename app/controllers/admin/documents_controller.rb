class Admin::DocumentsController < ApplicationController
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
  #   IMPORTANT NOTE!
  # 
  #   The `load_and_authorize_resource` method automagically creates the instance 
  #   for each method below.
  # 
  #   They remain in place but commented out for the sake of being explicit
  # 
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

  
  load_and_authorize_resource


  layout 'portal'

  def index
    @documents =  @documents.page(params[:page]).per(PER_PAGE)

  end

  def new
    # @document = Document.new
  end

  def create
    # @document = Document.new(params[:user])
    if @document.save
      redirect_to admin_documents_path , notice: 'Document was successfully created.'
    else
      render action: 'new'
    end
  end

  def edit
    
  end

  def update
    if @document.update_attributes(params[:document])
      redirect_to admin_documents_path, notice: 'Document was successfully updated.'
    else
      render action: 'edit'
    end
  end

  def destroy
    @document.destroy
    redirect_to admin_documents_path, notice: 'Document was successfully deleted.'
  end

  def financials
    @documents = Document.financials.page(params[:page]).per(PER_PAGE)
    render action: 'index'
  end

  def reports
    @documents = Document.reports.page(params[:page]).per(PER_PAGE)
    render action: 'index'
  end

  def misc
    @documents = Document.misc.page(params[:page]).per(PER_PAGE)
    render action: 'index'
  end


end
