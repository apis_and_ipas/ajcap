class User < ActiveRecord::Base
  rolify

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many  :messages,
              :as => :sender,
              :class_name => 'Message',
              :conditions => {:hidden_at => nil},
              :order => 'messages.created_at DESC'
              
  has_many  :received_messages,
              :as => :receiver,
              :class_name => 'MessageRecipient',
              :include => :message,
              :conditions => ['message_recipients.hidden_at IS NULL AND messages.state = ?', 'sent'],
              :order => 'messages.created_at DESC'

  attr_accessible :email, :username, :password, :password_confirmation, :remember_me, :is_admin
  attr_accessor :is_admin

  validates :email,    :presence => true, :uniqueness => true
  validates :username, :presence => true
  validates :password, :presence => true, :on => :create

  before_save :update_role_if_applicable

  def admin?
    has_role? :admin
  end

  def unsent_messages
    messages.with_state(:unsent)
  end

  def sent_messages
    messages.with_states(:queued, :sent)
  end

  def unread_messages_count
    # MessageRecipient.
    #   joins(:message).
    #   where(
    #     :receiver_id => self.id, 
    #     :receiver_type => 'User', 
    #     :hidden_at => nil,
    #     'messages.state' => 'unread').
    #   count
    self.received_messages.where(state:'unread').count
  end

  def documents
    
  end

  private

    def update_role_if_applicable 
      return unless is_admin.present?

      remove_role :admin if admin? && is_admin == '0'
      add_role :admin    if !admin? && is_admin == '1'  
    end

end
