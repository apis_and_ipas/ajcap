class Document < ActiveRecord::Base
  DOC_CATEGORIES = {
    'Financials' => 'financials',
    'Reports' => 'reports',
    'Miscellaneous' => 'misc'
  }

  mount_uploader :file, FileUploader

  before_save :update_asset_attributes

  has_many :messages, :through => :message_documents
  has_many :message_documents

  attr_accessible :title, :category, :file, :content_type

  default_scope order('created_at DESC')
  scope :reports, where(:category => 'reports')
  scope :financials, where(:category => 'financials')
  scope :misc, where(:category => 'misc')

  validates :title, :presence => true, :uniqueness => true
  validates :file, :presence => true, :on => :create

  def to_params
    self.title
  end

  private
    def update_asset_attributes
      if file.present? && file_changed?
        self.content_type = file.file.content_type
      end
    end
end
