class MessageDocument < ActiveRecord::Base
  belongs_to :message
  belongs_to :document
  attr_accessible :document_id, :message_id
end
