class Message < ActiveRecord::Base
  belongs_to :sender, :polymorphic => true
  has_many :recipients, :class_name => 'MessageRecipient', 
                        :order => 'kind DESC, position ASC', 
                        :dependent => :destroy

  has_many :message_documents
  has_many :documents, :through => :message_documents

  validates :state, :presence => true
  validates :sender_id, :presence => true
  validates :sender_type, :presence => true

  attr_accessible :subject, :body, :to, :cc, :bcc, :sender_type, :sender_id, :type, :document_ids

  after_save :update_recipients

  
  default_scope order('created_at DESC')
  scope :visible, where( :hidden_at => nil )

  state_machine :state, :initial => :unsent do
    event :queue do
      transition :unsent => :queued, :if => :has_recipients?
    end

    event :deliver do
      transition [:unsent, :queued] => :sent, :if => :has_recipients?
    end
  end

  state_machine :hidden_at, :initial => :visible do

    event :hide do
      transition all => :hidden
    end

    event :unhide do
     transition all => :visible
    end

    state :visible, :value => nil
    state :hidden, :value => lambda {Time.now}, :if => lambda {|value| value}
  end



  def to(*receivers)
    receivers(receivers, 'to')
  end
  alias_method :to=, :to
  
  def cc(*receivers)
    receivers(receivers, 'cc')
  end
  alias_method :cc=, :cc
  
  def bcc(*receivers)
    receivers(receivers, 'bcc')
  end
  alias_method :bcc=, :bcc
  
  def forward
    message = self.class.new(:subject => subject, :body => body)
    message.sender = sender
    message
  end

  def reply
    message = self.class.new(:subject => subject, :body => body)
    message.sender = sender
    message.to(to)
    message
  end
  
  def reply_to_all
    message = reply
    message.cc(cc)
    message.bcc(bcc)
    message
  end

  # def attachments
  #   @attachments
  # end

  # def add_attachment(*files)
  #   @attachments = []
  #   files.each do |file|
  #     @attachments.push file
  #   end
  # end
  
  private
    # Create/destroy any receivers that were added/removed
    def update_recipients
      if @receivers
        @receivers.each do |kind, receivers|
          receivers = receivers.reject { |r| r.blank? }.map { |r| User.find(r) }

          kind_recipients    = recipients.select {|recipient| recipient.kind == kind}
          new_receivers      = receivers - kind_recipients.map(&:receiver)
          removed_recipients = kind_recipients.reject { |recipient| receivers.include?(recipient.receiver) }
          
          recipients.delete(*removed_recipients) if removed_recipients.any?

          new_receivers.each do |receiver| 
            self.recipients.create!(:receiver => receiver, :kind => kind)
          end
        end
        
        @receivers = nil
      end
    end

    
    # Does this message have any recipients on it?
    def has_recipients?
      (to + cc + bcc).any?
    end
    
    # Creates new receivers or gets the current receivers for the given kind (to, cc, or bcc)
    def receivers(receivers, kind)
      if receivers.any?
        (@receivers ||= {})[kind] = receivers.flatten.compact
      else
        @receivers && @receivers[kind] || recipients.select {|recipient| recipient.kind == kind}.map(&:receiver)
      end
    end
end
