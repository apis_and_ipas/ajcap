module UserHelper

  def user_admin(user)
    '<i style="font-style: italic; color:red;">admin</i>'.html_safe if user.admin?
  end

  def user_name(user)
    icon = user.admin? ? 'icon-user-md' : 'icon-user'
    icon_helper(icon) + " #{user.username}"
  end

end