module DocumentHelper

  def doc_title_link(doc)
    link_to icon_helper('icon-file-alt') + ' &nbsp;'.html_safe + title_trunc(doc) , download_path(doc), :target => '_blank' 
  end

  def title_trunc(doc)
    truncate(doc.title, :ommision => "...", :length => 30) 
  end
  
end