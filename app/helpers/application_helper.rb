module ApplicationHelper

	def title(page_title)
	  content_for :title, page_title.to_s
	end

  def copyright_year
     start_copyright = 2013
     now = Time.new
     current_year = now.year

    if start_copyright >= current_year
      "#{current_year}"
    else
      "#{start_copyright} - #{current_year}"
    end
  end

  def icon_helper(icon_name)
    "<i class=#{icon_name}></i>".html_safe
  end

  def split_button_helper(icon_name)
    "<span class=#{icon_name}></span>".html_safe
  end


end
