module MessageHelper 

  # Adds red dot with # of unread messages
  def unread_count
    "<span class='unread-count'>#{current_user.unread_messages_count}</span>".html_safe if current_user.unread_messages_count > 0
  end

  def attachment_indicator(msg)
    if msg.documents.length > 0
      "<i class='icon-paper-clip'></i>".html_safe 
    end
  end

  # Adds blue dot in message list
  def unread_indicator(msg)
      @msg = MessageRecipient.where(:message_id => msg.id, :receiver_id => current_user.id).first
      output = "<span class='unread'>#{icon_helper('icon-circle')}</span>".html_safe
      else_put = "<span class='read'>#{icon_helper('icon-circle')}</span>".html_safe
      # output if @msg.state == 'unread'
      @msg.state == 'unread' ? output : else_put
  end

  # Changes the label for the Submit button the Message form depending on the state of the record.
  def msg_button_label(msg)
    msg.new_record? ? 'Send Message' : 'Update Message'
  end

 


end
