
(function() {

    $(function(){
        initLightBox();
    });

    function initLightBox(){
        $('.prop > div[data-property-name]').click(function(e){
            e.preventDefault();
            var el = $(this);

            buildLightbox(el);
        });
        $(document).trigger('lightbox.init');
    }

    function buildLightbox(el){
        log('buildLightbox fired', el);

        // Grab contents for lightbox from DOM elements on index page.
        var thisProp = el.find('.prop-info'),
            thisPropTitle = thisProp.find('h3').text(),
            thisPropDesc = thisProp.find('p').text();


        $('.lightbox-title h3').html(thisPropTitle);
        $('.lightbox-desc p').html(thisPropDesc);
        
        $('.prop-link').click(function(e){
            e.stopPropagation();
        });

        buildSlideshow(el);

        $('.site-header').hide();
        $('.lightbox').show();

        // Set up click event for closing modal and reset contents of slideshow
        $('body').on('click','.modal-close, .mobile-close', function(e) {
            setCloseEvents(e);
        });
    }

    function buildSlideshow(el) {
        //Destroy any previous incarnations of the slideshow
        $('.lightbox-slides').cycle('destroy');

        // Grab data from clicked dom elements
        var propName = el.data('property-name'),
            propPhotoCount = el.data('photos'),
            slideElements = '';

        log(propName, propPhotoCount);



        // Build up the DOM for the new slideshow
        for(currentSlide = 0; currentSlide < propPhotoCount; ++currentSlide) {
            slideElements += '<img src="assets/'+ propName +'/slide' + (currentSlide + 1)  + '.jpg" />';
        }
        $('.lightbox-slides').append(slideElements);

        // Kick off that slideshow
        $('.lightbox-slides').cycle({
            speed: 600,
            swipe: true
        }).cycle('pause');

        $('.lightbox').bind('mousewheel', function(event, delta) {
            // event.preventDefault();
            if (delta > 25) {
                log('scrolling down');
                $('.lightbox-slides').cycle('prev');
            } else if (delta > -25){
                log('scrolling up');
                 $('.lightbox-slides').cycle('next');
            }
            return false;
        });

        $(document).on('keydown', function(e){
            switch(e.which) {
                case 37: // left
                    log('left!');
                    $('.lightbox-slides').cycle('prev');
                break;

                case 38: // up
                    log('up!');
                    $('.lightbox-slides').cycle('prev');
                break;

                case 39: // right
                    log('right!');
                    $('.lightbox-slides').cycle('next');
                break;

                case 40: // down
                    log('down');
                    $('.lightbox-slides').cycle('next');
                break;

                default: return; // exit this handler for other keys
            }
            e.preventDefault();
        });
    }

    function setCloseEvents(e){
        e.stopPropagation();
        $('.lightbox').hide();
        $('.lightbox-slides').html('');
        $('.site-header').show();
        $('.lightbox').unbind('mousewheel');
        $(document).off('keydown');
    }

})();
