
(function() {
    $(document).ready( function(){

       // if (matchMedia('screen and (min-width: 1025px)').matches ) {
       //      allowParallax();
       // }

        $(window).resize(function(){
            var wHeight = $(window).height(),
                wWidth  = $(window).width();

            $('.placard').css({
                'min-height': wHeight,
                'min-width': wWidth
            });
            $('#contact-us').css({
                'height': (wHeight - 150)
            });


            if (wWidth >= 1025) {
                allowParallax();
            }

        }).trigger('resize');


        $(window).on( 'scroll', function(){
            if ($(window).scrollTop() > 25 ){
                $('.site-header').slideDown();
            }
             if (scrollBottom()) {
                $('.site-footer').slideDown();
            } else {
                $('.site-footer').slideUp();
            }
        });

        });

    function allowParallax(){
        log('allowParallax fired!');
        $.stellar({
            horizontalScrolling: false,
            verticalOffset: 0,
            horizontalOffset: 0
        });
    }

    function scrollBottom() {
        return ($(window).innerHeight() + $(window).scrollTop()) >= $('body').height();
    }

})();
