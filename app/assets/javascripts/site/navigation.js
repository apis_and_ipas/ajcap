 $(function(){


    $('.placard').waypoint(function(event, direction){
        dataslide = $(this).attr('data-slide');

        if (direction === 'down') {
            $('.active-nav').removeClass('active-nav');
            $('.navigation li[data-slide="' + dataslide + '"]').addClass('active-nav');
        }
        else {
            $('.active-nav').removeClass('active-nav');
            $('.navigation li[data-slide="' + dataslide + '"]').addClass('active-nav');
        }
    });

    function goToByScroll(dataslide) {
        console.log(dataslide);
        $('html,body').animate({
            scrollTop: $('.placard[data-slide="' + dataslide + '"]').offset().top
        }, 2000, 'easeInOutQuint');
    }

    $('.navigation').find('li:not(.login), h1, .scroll-link').click(function (e) {
        e.preventDefault();
        dataslide = $(this).attr('data-slide');
        goToByScroll(dataslide);
    });


  

 });
