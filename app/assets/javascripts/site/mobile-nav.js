$(function() {
  var $menu = $('#menu'),
  $menulink = $('.mobile-nav');

  // Toggle active state 
  $menulink.click(function(e){
    log('mobile-nav clicked!');
    $menulink.toggleClass('active');
    $menu.slideToggle().toggleClass('active');
    e.preventDefault();
  });


  // Hide mobile nav after a link is clicked
  $('#menu a').click(function(e){
    $menulink.toggleClass('active');
    $menu.slideToggle().toggleClass('active');
    $('.lightbox').hide();
  });


  $('.site-header a').click(function(){
      $('.lightbox').hide();
  });

});
