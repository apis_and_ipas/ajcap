//= require jquery
//= require jquery_ujs
//= require chosen-jquery
//= require shared/util
//= require shared/common
//= require portal/autosave
//= require portal/year-filter

$(function() {
  var $menu = $('#menu'),
  $menulink = $('.mobile-nav');

  // Toggle active state 
  $menulink.click(function(e){
    log('mobile-nav clicked!');
    $menulink.toggleClass('active');
    $menu.slideToggle().toggleClass('active');
    e.preventDefault();
  });


  // Hide mobile nav after a link is clicked
  $('#menu a').click(function(e){
    $menulink.toggleClass('active');
    $menu.slideToggle().toggleClass('active');
  });


  $('#message_to').attr('data-placeholder','Select recipients...').chosen({
    allow_single_deselect: true,
    no_results_text: 'No results matched'
  });

    $('#message_document_ids').attr('data-placeholder','Select attchments...').chosen({
    allow_single_deselect: true,
    no_results_text: 'No results matched'
  });



});
