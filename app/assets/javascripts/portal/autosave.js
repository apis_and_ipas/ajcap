$(function(){
  log('Autosaver Loaded!');

  // Recovery
  var autosave_body = localStorage.getItem('ajcp.message.body'),
      autosave_subject = localStorage.getItem('ajcp.message.subject'),
      body  = JSON.parse(autosave_body),
      subject = JSON.parse(autosave_subject);

    if (autosave_body && autosave_subject){ 

      $('#message_body').text(body);
      $('#message_subject').val(subject);

    }
  //Autosaver!

  $('#message_body').change(function(){
    var body = $(this).val();
    localStorage.setItem('ajcp.message.body', JSON.stringify(body));

  });

  $('#message_subject').change(function(){
    var subject = $(this).val();
    localStorage.setItem('ajcp.message.subject', JSON.stringify(subject));

  });

  $('#new_message').submit(function(){
      localStorage.removeItem('ajcp.message.subject');
      localStorage.removeItem('ajcp.message.body');
  });

});