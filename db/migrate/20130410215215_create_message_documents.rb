class CreateMessageDocuments < ActiveRecord::Migration
  def change
    create_table :message_documents do |t|
      t.integer :message_id
      t.integer :document_id

      t.timestamps
    end
  end
end
