class ChangeSubjectToStringOnMessage < ActiveRecord::Migration
  def up
    change_table :messages do |t|
      t.change :subject, :string
    end
  end

  def down
  end
end
