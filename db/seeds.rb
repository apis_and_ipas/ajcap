require 'cancan'
require 'rolify'

seed_users = [
              { 
                username: 'Dan Schor', 
                email: 'ds@ajcpt.com',
                password: 'danajcpt',
                password_confirmation: 'danajcpt'
              },
              { 
                username: 'Doejo Admin', 
                email: 'bryan@doejo.com',
                password: '3128broadway',
                password_confirmation: '3128broadway'
              }
            ]

puts "Creating seed users!"
User.create(seed_users)


puts "Creating admin user for AJCPT!"
admin = User.find 1
admin.add_role :admin

puts "Creating admin user for Doejo!"
doejo_admin = User.find 2
doejo_admin.add_role :admin


puts admin.has_role? :admin
puts doejo_admin.has_role? :admin
