SHUTTLE_CONFIG = './config/shuttle.yml'

#  Bare minimum shuttle.yml file for Rails staging deployment
# app:
#   name: (app name)
#   strategy: rails
#   git: git@gitslice.com:(app_name).git
# 
# targets:
#   staging:
#     host: doejocloud.com
#     user: (vm_name)
#     password: 
#     deploy_to: /home/apps/www

#  TODO:  set up a single deploy task which accepts param of environment name?

namespace :deploy do

	task :config_exists => :environment do
		if  File.exists?(SHUTTLE_CONFIG)
			puts "Shuttle config file found! "
		else
			puts  "ERROR: Please place your shuttle.yml file inside the config directory!"
		end
	end

	desc "Deploy to the staging environment"
	task :staging => :config_exists do
	   `shuttle -f #{SHUTTLE_CONFIG} staging deploy`
	end

	desc "Deploy to the production environment"
	task :production => :config_exists do
       `shuttle -f #{SHUTTLE_CONFIG} deploy`
	end

end

 # by default, deploy to staging
task :deploy => 'deploy:staging'
