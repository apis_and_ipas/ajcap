AjCapital::Application.routes.draw do

  devise_for :users#, :controllers => { :registrations => :registrations }
  
  namespace :admin do
    resources :users
    resources :messages
    resources :documents
    get '/financials' => 'documents#financials'
    get '/reports' => 'documents#reports'
    get '/misc' => 'documents#misc'
  end

  root :to => 'site#index'
  match 'user_root' => 'messages#index'
  match '/portal' => 'messages#index', :via => :get
  match '/message/:id' => 'messages#show', :as => :message, :via => :get
  match '/message/:id/hide' => 'messages#hide', :as => :hide_message#, :via => :post
  
  match '/documents' => 'documents#index', :via => :get 
  get '/documents/financials' => 'documents#financials'
  get '/documents/reports' => 'documents#reports'
  get '/documents/misc' => 'documents#misc'
  get '/download/:id' => 'documents#download',:as => :download
end
