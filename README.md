# AJ Capital Partners

For Local Development:
    You must create a config/local_env.yml file which will look like this:
        GMAIL_DOMAIN: doejo.com
        GMAIL_USERNAME: YOURNAME@doejo.com
        GMAIL_PASSWORD: YOURPASSWORD

    This has been added to .gitignore, so it will remain secure-ish.


For password resets, as of now client must click 'Forgot password' in login screen and enter thier associated email 
address to be sent a link to set a new password.

## Deployment

To deploy to staging:

```
bundle exec shuttle staging deploy
```

To deploy to production:

```
bundle exec shuttle production deploy
```