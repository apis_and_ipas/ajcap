# This file is used by Rack-based servers to start the application.

require ::File.expand_path('../config/environment',  __FILE__)

require 'rack/revision'

use Rack::Revision
run AjCapital::Application
