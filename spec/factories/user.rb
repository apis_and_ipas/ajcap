FactoryGirl.define do

  factory :user do
    username 'myUsername123'
    email 'example@example.com'
    password 'changeme'
    password_confirmation 'changeme'
    id '1'
    factory :admin do
        after(:create) {|user| user.add_role(:admin)}
    end

  end


  factory :other_user, class: User do
    username 'otherUser'
    email 'example2@example.com'
    id '2'
    password 'changeme'
    password_confirmation 'changeme'
  end

end
