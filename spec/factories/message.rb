# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do

  factory :message do
    subject "Example Subject"
    body "Example body text is slightly longer than the example subject text."
    association :sender, :factory => :user
  end


  # factory :message_receiver do
  # end
end
