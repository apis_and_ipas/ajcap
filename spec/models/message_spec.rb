require 'spec_helper'

describe Message do

  # describe 'adding attaching documents' do
  #   let!(:sender) { Fabricate(:user)}

  #   context 'for attaching a single document' do
  #     let!(:user1) { Fabricate(:user) }
  #     let!(:user2) { Fabricate(:user) }
  #     let!(:doc)   { Fabricate(:document) }

  #     let(:message) do
  #       msg = Fabricate.build(:message)
  #       msg.sender = sender
  #       msg.to(user1, user2)
  #       msg.save
  #       msg
  #     end

      

  #   end

  # end


  describe 'sending messages' do
    let!(:sender) { Fabricate(:user) }

    context 'for multiple recipients' do
      let!(:user1)  { Fabricate(:user) }
      let!(:user2)  { Fabricate(:user) }

      let(:message) do
        msg = Fabricate.build(:message)
        msg.sender = sender
        msg.to(user1, user2)
        msg.save
        msg
      end

      before do
        message.deliver
      end

      it 'creates a new message for 2 recipients' do
        user1.received_messages.should_not be_empty
        user2.received_messages.should_not be_empty
      end

      it { should respond_to :hide }
      it { should respond_to :unhide }
      it { should respond_to :fire_events }
    end
  end
end
